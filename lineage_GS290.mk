#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/product_launched_with_p.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from GS290 device
$(call inherit-product, device/gigaset/GS290/device.mk)

PRODUCT_DEVICE := GS290
PRODUCT_NAME := lineage_GS290
PRODUCT_BRAND := gigaset
PRODUCT_MODEL := GS290
PRODUCT_MANUFACTURER := Gigaset

PRODUCT_GMS_CLIENTID_BASE := android-gigaset

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=GS290_EEA \
    PRIVATE_BUILD_DESC="full_k63v2_64_bsp-user-10-QP1A.190711.020-1676368022-release-keys"

BUILD_FINGERPRINT := Gigaset/GS290_EEA/GS290:10/QP1A.190711.020/1676368022:user/release-keys
