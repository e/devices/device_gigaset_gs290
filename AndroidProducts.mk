#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_GS290.mk

COMMON_LUNCH_CHOICES := \
    lineage_GS290-user \
    lineage_GS290-userdebug \
    lineage_GS290-eng
